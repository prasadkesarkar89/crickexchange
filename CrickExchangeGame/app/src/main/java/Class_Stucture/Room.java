package Class_Stucture;

/**
 * Created by Admin on 3/11/2018.
 */

import com.google.gson.annotations.SerializedName;

public class Room {
    @SerializedName("id")
    private String id;
    @SerializedName("room_name")
    private String room_name;
    @SerializedName("total_winnings")
    private String total_winnings;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoom_name() {
        return room_name;
    }

    public void setRoom_name(String room_name) {
        this.room_name = room_name;
    }

    public String getTotal_winnings() {
        return total_winnings;
    }

    public void setTotal_winnings(String total_winnings) {
        this.total_winnings = total_winnings;
    }

    public String getTotal_winners() {
        return total_winners;
    }

    public void setTotal_winners(String total_winners) {
        this.total_winners = total_winners;
    }

    public String getEntry_fee() {
        return entry_fee;
    }

    public void setEntry_fee(String entry_fee) {
        this.entry_fee = entry_fee;
    }

    public String getMax_team_allowed() {
        return max_team_allowed;
    }

    public void setMax_team_allowed(String max_team_allowed) {
        this.max_team_allowed = max_team_allowed;
    }

    public String getTerms_andcondition() {
        return terms_andcondition;
    }

    public void setTerms_andcondition(String terms_andcondition) {
        this.terms_andcondition = terms_andcondition;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    @SerializedName("total_winners")
    private String total_winners;
    @SerializedName("entry_fee")
    private String entry_fee;
    @SerializedName("max_team_allowed")
    private String max_team_allowed;
    @SerializedName("terms_and_condition")
    private String terms_andcondition;

    @SerializedName("created_date")
    private String created_date;
    @SerializedName("updated_date")
    private String updated_date;
}
