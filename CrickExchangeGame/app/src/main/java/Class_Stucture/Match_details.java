package Class_Stucture;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 3/4/2018.
 */

public class Match_details {
    @SerializedName("match_id")
    private String match_id;
    @SerializedName("match_name")
    private String match_name;
    @SerializedName("match_date")
    private String match_date;
    @SerializedName("team_one_name")
    private String team_one_name;
    @SerializedName("team_two_name")
    private String team_two_name;
    @SerializedName("team_one_logo")
    private String team_one_logo;
    @SerializedName("team_two_logo")
    private String team_two_logo;
    @SerializedName("team_one_players")
    private List<Player> team_one_players;
    @SerializedName("team_two_players")
    private List<Player>  team_two_players;



    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getMatch_name() {
        return match_name;
    }

    public void setMatch_name(String match_name) {
        this.match_name = match_name;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getTeam_one_name() {
        return team_one_name;
    }

    public void setTeam_one_name(String team_one_name) {
        this.team_one_name = team_one_name;
    }

    public String getTeam_two_name() {
        return team_two_name;
    }

    public void setTeam_two_name(String team_two_name) {
        this.team_two_name = team_two_name;
    }

    public String getTeam_one_logo() {
        return team_one_logo;
    }

    public void setTeam_one_logo(String team_one_logo) {
        this.team_one_logo = team_one_logo;
    }

    public String getTeam_two_logo() {
        return team_two_logo;
    }

    public void setTeam_two_logo(String team_two_logo) {
        this.team_two_logo = team_two_logo;
    }

    public List<Player> getTeam_one_players() {
        return team_one_players;
    }

    public void setTeam_one_players(List<Player> team_one_players) {
        this.team_one_players = team_one_players;
    }

    public List<Player> getTeam_two_players() {
        return team_two_players;
    }

    public void setTeam_two_players(List<Player> team_two_players) {
        this.team_two_players = team_two_players;
    }
}
