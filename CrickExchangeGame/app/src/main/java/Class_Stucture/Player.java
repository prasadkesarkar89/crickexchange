package Class_Stucture;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 3/2/2018.
 */

public class Player {
    @SerializedName("player_id")
    private int player_id;
    @SerializedName("player_name")
    private String player_name;
    @SerializedName("player_short_name")
    private String player_short_name;
    @SerializedName("player_role")
    private String player_role;
    @SerializedName("country_id")
    private String country_id;
    @SerializedName("country_name")
    private String country_name;
    @SerializedName("flag_32")
    private String flag_32;
    @SerializedName("sports_id")
    private String sports_id;
    @SerializedName("sports_name")
    private String sports_name;


    public boolean isSelect() {
        return isSelect;
    }

    public void setSelect(boolean select) {
        isSelect = select;
    }

    private boolean isSelect;

    public int getPlayer_id() {
        return player_id;
    }

    public void setPlayer_id(int player_id) {
        this.player_id = player_id;
    }

    public String getPlayer_name() {
        return player_name;
    }

    public void setPlayer_name(String player_name) {
        this.player_name = player_name;
    }

    public String getPlayer_short_name() {
        return player_short_name;
    }

    public void setPlayer_short_name(String player_short_name) {
        this.player_short_name = player_short_name;
    }

    public String getPlayer_role() {
        return player_role;
    }

    public void setPlayer_role(String player_role) {
        this.player_role = player_role;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getFlag_32() {
        return flag_32;
    }

    public void setFlag_32(String flag_32) {
        this.flag_32 = flag_32;
    }

    public String getSports_id() {
        return sports_id;
    }

    public void setSports_id(String sports_id) {
        this.sports_id = sports_id;
    }

    public String getSports_name() {
        return sports_name;
    }

    public void setSports_name(String sports_name) {
        this.sports_name = sports_name;
    }
}
