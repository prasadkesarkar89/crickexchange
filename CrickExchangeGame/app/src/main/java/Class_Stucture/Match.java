package Class_Stucture;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;
import java.util.SimpleTimeZone;

/**
 * Created by Admin on 3/2/2018.
 */

public class Match {
    @SerializedName("match_id")
    private String match_id;
    @SerializedName("match_name")
    private String match_name;
    @SerializedName("match_date")
    private String match_date;
    @SerializedName("team_one_name")
    private String team_one_name;
    @SerializedName("team_two_name")
    private String team_two_name;
    @SerializedName("team_one_logo")
    private String team_one_logo;
    @SerializedName("team_two_logo")
    private String team_two_logo;


    private int typematch;

    public int getTypematch(){
        return typematch;
    }

    public void setTypematch(int typematch)
    {
        this.typematch  = typematch;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public String getMatch_name() {
        return match_name;
    }

    public void setMatch_name(String match_name) {
        this.match_name = match_name;
    }

    public String getMatch_date() {
        return match_date;
    }

    public void setMatch_date(String match_date) {
        this.match_date = match_date;
    }

    public String getTeam_one_name() {
        return team_one_name;
    }

    public void setTeam_one_name(String team_one_name) {
        this.team_one_name = team_one_name;
    }

    public String getTeam_two_name() {
        return team_two_name;
    }

    public void setTeam_two_name(String team_two_name) {
        this.team_two_name = team_two_name;
    }

    public String getTeam_one_logo() {
        return team_one_logo;
    }

    public void setTeam_one_logo(String team_one_logo) {
        this.team_one_logo = team_one_logo;
    }

    public String getTeam_two_logo() {
        return team_two_logo;
    }

    public void setTeam_two_logo(String team_two_logo) {
        this.team_two_logo = team_two_logo;
    }


}
