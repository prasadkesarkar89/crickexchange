package Class_Stucture;

/**
 * Created by Admin on 2/24/2018.
 */
import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("id")
    private int id;
    @SerializedName("username")
    private String username;
    @SerializedName("email_id")
    private String email_id;
    @SerializedName("mobile_no")
    private String mobile_no;


    public boolean isLogin() {
        return isLogin;
    }

    public void setLogin(boolean login) {
        isLogin = login;
    }

    private boolean isLogin;





    private String is_active,created_date,updated_date;

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIs_active() {
        return is_active;
    }

    public void setIs_active(String is_active) {
        this.is_active = is_active;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getUpdated_date() {
        return updated_date;
    }

    public void setUpdated_date(String updated_date) {
        this.updated_date = updated_date;
    }

    public User(int id, String username, String email, String mobile,boolean isLogin) {
        this.id = id;
        this.username = username;
        this.email_id = email;
        this.mobile_no = mobile;
        this.isLogin = isLogin;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email_id;
    }

    public String getMobile() {
        return mobile_no;
    }
}
