package Class_Stucture;

/**
 * Created by Admin on 3/11/2018.
 */
import com.google.gson.annotations.SerializedName;
public class PlayerMatch {
    String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIs_captain() {
        return is_captain;
    }

    public void setIs_captain(String is_captain) {
        this.is_captain = is_captain;
    }

    public String getIs_vc_captain() {
        return is_vc_captain;
    }

    public void setIs_vc_captain(String is_vc_captain) {
        this.is_vc_captain = is_vc_captain;
    }

    String is_captain;
    String is_vc_captain;
}
