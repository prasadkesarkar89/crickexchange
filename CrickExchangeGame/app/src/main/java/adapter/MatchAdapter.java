package adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cricxchange.admin.crickexchangegame.R;

import java.util.ArrayList;
import java.util.List;

import Class_Stucture.Match;
import Utility.URLs;

/**
 * Created by Admin on 3/2/2018.
 */

public class MatchAdapter extends RecyclerView.Adapter<MatchAdapter.MyViewHolder> {

    ArrayList<Match> matchList_array;

    Context con ;
    public MatchAdapter(ArrayList<Match> matchList_array, Context con)
    {
        this.matchList_array = matchList_array;
        this.con = con;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_row,parent, false);
        return new MyViewHolder(v);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        Match mObject = matchList_array.get(position);
        Glide.with(con)
                .load(URLs.IMAGE_LOADER+""+mObject.getTeam_one_logo())
                .into(holder.team_one_logo);
        holder.team_one_name.setText(mObject.getTeam_one_name());
        holder.match_name.setText(mObject.getMatch_name());
        Glide.with(con)
                .load(URLs.IMAGE_LOADER+""+mObject.getTeam_one_logo())
                .into(holder.team_two_logo);
         holder.team_two_name.setText(mObject.getTeam_two_name());

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
      public ImageView team_one_logo;
      public TextView team_one_name;
      public TextView match_name;
      public TextClock timer;
      public ImageView team_two_logo;
      public TextView team_two_name;

        public MyViewHolder(View view) {
            super(view);
            team_one_logo = (ImageView) view.findViewById(R.id.team_one_logo);
            team_one_name = (TextView) view.findViewById(R.id.team_one_name);
            match_name = (TextView) view.findViewById(R.id.match_name);
            timer = (TextClock) view.findViewById(R.id.timer);
            team_two_logo = (ImageView) view.findViewById(R.id.team_two_logo);
            team_two_name = (TextView) view.findViewById(R.id.team_one_name);
        }
    }
}
