package adapter;

/**
 * Created by Admin on 3/4/2018.
 */

import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cricxchange.admin.crickexchangegame.R;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import Class_Stucture.Match;
import Class_Stucture.Player;
import Utility.AllFunctions;
import Utility.URLs;

public class ListAdapter_match extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Match> objects;
    private int selectedPosition = -1;
    public ListAdapter_match(Context context, ArrayList<Match> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.match_row_temp, parent, false);
        }
        try {
            ImageView team_one_logo;
            TextView team_one_name;
            TextView match_name;
            TextView timer;
            ImageView team_two_logo;
            TextView team_two_name;

            team_one_logo = (ImageView) view.findViewById(R.id.team_one_logo);
            team_one_name = (TextView) view.findViewById(R.id.team_one_name);
            match_name = (TextView) view.findViewById(R.id.match_name);
            timer = (TextView) view.findViewById(R.id.timer);
            team_two_logo = (ImageView) view.findViewById(R.id.team_two_logo);
            team_two_name = (TextView) view.findViewById(R.id.team_two_name);


            Match match = objects.get(position);
            Glide.with(ctx)
                    .load(URLs.IMAGE_LOADER + "" + match.getTeam_one_logo())
                    .into(team_one_logo);
            team_one_name.setText(match.getTeam_one_name());
            match_name.setText(match.getMatch_name());
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            Calendar cal = Calendar.getInstance();


//            Date today  =  dateFormat.parse(cal.toString());

            Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;

            createdConvertedDate = dateFormat.parse(match.getMatch_date().toString());
//        holder.timer.set
            int days = (int) (createdConvertedDate.getTime() / (1000*60*60*24));
            int currentdays = (int) (cal.getTimeInMillis() / (1000*60*60*24));
            long daytoshow = days - currentdays;

            if (match.getTypematch() == 3) {
                if(daytoshow < 2) {
                    reverseTimer(createdConvertedDate.getTime(), timer, cal.getTimeInMillis());
                }
                else
                {
                    DateFormat originalFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                    timer.setText(originalFormat.format(createdConvertedDate));
                }
            }
//        AllFunctions.getCountOfDays(match.getMatch_date().toString(),"");
//        reverseTimer(cal.getTimeInMillis(),holder.timer);
            Glide.with(ctx)
                    .load(URLs.IMAGE_LOADER + "" + match.getTeam_one_logo())
                    .into(team_two_logo);
            team_two_name.setText(match.getTeam_two_name());

        }catch (Exception e)
        {

        }
        return view;
    }


    public void reverseTimer(long Seconds, final  TextView tv,final long todayTime) {

        new CountDownTimer(Seconds, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);


                int days = (int) (millisUntilFinished / (1000*60*60*24));
                int currentdays = (int) (todayTime / (1000*60*60*24));

                long days1 = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                long daytoshow = days - currentdays;
                days1 = days1 /60 * 60;
                days1 = days1 /24;
                tv.setText(daytoshow+"d"+
                        ((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toHours(todayTime) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(todayTime)))) +"h"+
                                ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toMinutes(todayTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(todayTime))))+"m"+
                                        (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))+"s"))));
            }

            public void onFinish() {
                tv.setText("Completed");
            }
        }.start();
    }





}

