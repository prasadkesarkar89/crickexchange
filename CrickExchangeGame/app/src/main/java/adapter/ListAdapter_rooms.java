package adapter;

/**
 * Created by Admin on 3/4/2018.
 */

import android.content.Context;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cricxchange.admin.crickexchangegame.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import Class_Stucture.Match;
import Class_Stucture.Room;
import Utility.URLs;

public class ListAdapter_rooms extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Room> objects;
    private int selectedPosition = -1;
    public ListAdapter_rooms(Context context, ArrayList<Room> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.room_row_temp, parent, false);
        }
        try {
            TextView roomname;
            TextView totalwinnings;
            TextView totalwinner;
            TextView etryfee;

            roomname = (TextView) view.findViewById(R.id.roomname);
            totalwinnings = (TextView) view.findViewById(R.id.totalwinnings);
            totalwinner = (TextView) view.findViewById(R.id.totalwinner);
            etryfee = (TextView) view.findViewById(R.id.etryfee);


            Room rm = objects.get(position);
            roomname.setText(rm.getRoom_name());
            totalwinnings.setText(rm.getTotal_winnings());
            totalwinner.setText(rm.getTotal_winners());
            etryfee.setText("Entry Fees: "+rm.getEntry_fee());

        }catch (Exception e)
        {

        }
        return view;
    }


    public void reverseTimer(long Seconds, final  TextView tv,final long todayTime) {

        new CountDownTimer(Seconds, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);


                int days = (int) (millisUntilFinished / (1000*60*60*24));
                int currentdays = (int) (todayTime / (1000*60*60*24));

                long days1 = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                long daytoshow = days - currentdays;
                days1 = days1 /60 * 60;
                days1 = days1 /24;
                tv.setText(daytoshow+"d"+
                        ((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toHours(todayTime) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(todayTime)))) +"h"+
                                ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toMinutes(todayTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(todayTime))))+"m"+
                                        (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))+"s"))));
            }

            public void onFinish() {
                tv.setText("Completed");
            }
        }.start();
    }





}

