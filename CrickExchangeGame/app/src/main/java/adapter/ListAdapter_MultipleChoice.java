package adapter;

/**
 * Created by Admin on 3/4/2018.
 */

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cricxchange.admin.crickexchangegame.R;

import java.util.ArrayList;

import Class_Stucture.Player;
import Utility.URLs;

public class ListAdapter_MultipleChoice extends BaseAdapter {
    Context ctx;
    LayoutInflater lInflater;
    ArrayList<Player> objects;
    public ListAdapter_MultipleChoice(Context context, ArrayList<Player> products) {
        ctx = context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Player p = getProduct(position);

        ((TextView) view.findViewById(R.id.player_name)).setText(p.getPlayer_name());
//        ((ImageView) view.findViewById(R.id.team_logo)).setImageResource(p.image);

        Glide.with(ctx)
                .load(URLs.IMAGE_LOADER + "" + p.getFlag_32())
                .into(((ImageView) view.findViewById(R.id.team_logo)));

        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
        cbBuy.setOnCheckedChangeListener(myCheckChangList);
        cbBuy.setTag(position);
        cbBuy.setChecked(p.isSelect());


        return view;
    }

    Player getProduct(int position) {
        return ((Player) getItem(position));
    }

    ArrayList<Player> getBox() {
        ArrayList<Player> box = new ArrayList<Player>();
        for (Player p : objects) {
            if (p.isSelect())
                box.add(p);
        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).setSelect(isChecked);
        }
    };
}

