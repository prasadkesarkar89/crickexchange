package adapter;

/**
 * Created by Admin on 3/4/2018.
 */

import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cricxchange.admin.crickexchangegame.CrickExchangeApplication;
import com.cricxchange.admin.crickexchangegame.R;

import Class_Stucture.Player;
import Utility.URLs;
import interfaces.GetPlayerCount;

public class ListAdapter extends BaseAdapter {
    Context ctx;
    GetPlayerCount gplayercnt;
    LayoutInflater lInflater;
    ArrayList<Player> objects;
    private int selectedPosition = -1;
    public ListAdapter(Context context, ArrayList<Player> products) {
        ctx = context;
        gplayercnt = (GetPlayerCount) context;
        objects = products;
        lInflater = (LayoutInflater) ctx
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        Player p = getProduct(position);

        ((TextView) view.findViewById(R.id.player_name)).setText(p.getPlayer_name());
//        ((ImageView) view.findViewById(R.id.team_logo)).setImageResource(p.image);

//        Glide.with(ctx)
//                .load(URLs.IMAGE_LOADER + "" + p.getFlag_32())
//                .into(((ImageView) view.findViewById(R.id.team_logo)));

        CheckBox cbBuy = (CheckBox) view.findViewById(R.id.cbBox);
        cbBuy.setOnCheckedChangeListener(myCheckChangList);
        cbBuy.setTag(position);
        cbBuy.setChecked(p.isSelect());

        if(p.getPlayer_role().equalsIgnoreCase("Wicket Keeper")) {
            if (position == selectedPosition) {
                cbBuy.setChecked(true);
            } else cbBuy.setChecked(false);

            cbBuy.setOnClickListener(onStateChangedListener(cbBuy, position));
        }
        return view;
    }


    private View.OnClickListener onStateChangedListener(final CheckBox checkBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkBox.isChecked()) {
                    selectedPosition = position;
                    getProduct(position).setSelect(true);
                    CrickExchangeApplication.teamList.add(getProduct(position));
                    gplayercnt.playerCount(1,getProduct(position).getPlayer_role());
                } else {
                    getProduct(position).setSelect(false);
                    CrickExchangeApplication.teamList.remove(CrickExchangeApplication.teamList.contains(getProduct(position)));
                    gplayercnt.playerCount(-1,getProduct(position).getPlayer_role());
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }
    Player getProduct(int position) {
        return ((Player) getItem(position));
    }

    ArrayList<Player> getBox() {
        ArrayList<Player> box = new ArrayList<Player>();
        for (Player p : objects) {
            if (p.isSelect())
                box.add(p);
        }
        return box;
    }

    OnCheckedChangeListener myCheckChangList = new OnCheckedChangeListener() {
        public void onCheckedChanged(CompoundButton buttonView,
                                     boolean isChecked) {
            getProduct((Integer) buttonView.getTag()).setSelect(isChecked);

            if (isChecked) {
                getProduct().setSelect(true);
                CrickExchangeApplication.teamList.add(getProduct(position));
                gplayercnt.playerCount(1,getProduct(position).getPlayer_role());
            } else {
                getProduct(position).setSelect(false);
                CrickExchangeApplication.teamList.remove(CrickExchangeApplication.teamList.contains(getProduct(position)));
                gplayercnt.playerCount(-1,getProduct(position).getPlayer_role());
                selectedPosition = -1;
            }
        }
    };
}

