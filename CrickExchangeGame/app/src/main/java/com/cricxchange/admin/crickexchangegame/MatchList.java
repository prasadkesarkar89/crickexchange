package com.cricxchange.admin.crickexchangegame;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Class_Stucture.Match;
import Class_Stucture.User;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;
import adapter.MatchAdapter;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MatchList.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MatchList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MatchList extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public ProgressBar progressBar2;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ArrayList<Match> mMatchArrayFixture = new ArrayList<Match>();
    public ArrayList<Match> mMatchArrayLive = new ArrayList<Match>();
    public ArrayList<Match> mMatchArrayResult = new ArrayList<Match>();
    public RecyclerView match_list;

    public MatchList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MatchList.
     */
    // TODO: Rename and change types and number of parameters
    public static MatchList newInstance(String param1, String param2) {
        MatchList fragment = new MatchList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_match_list, container, false);
       // progressBar2 = (ProgressBar) v.findViewById(R.id.progressBar2);
        match_list = (RecyclerView) v.findViewById(R.id.match_list);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        match_list.setLayoutManager(llm);
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }


    private void fetch_matches() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);




        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FETCH_MATCH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            mMatchArrayFixture.clear();
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                JSONArray match_array = obj.getJSONArray("matches_info");
                                 for (int i = 0 ; i< match_array.length(); i++)
                                 {
                                     JSONObject matchObj = match_array.getJSONObject(i);

                                     Match m_obje = gson.fromJson(matchObj.toString(), Match.class);
                                     mMatchArrayFixture.add(m_obje);
                                 }

                                MatchAdapter ca = new MatchAdapter(mMatchArrayFixture,getActivity());
                                match_list.setAdapter(ca);
                            }
                            else
                            {

                            }
//                            if(obj.get)
                        } catch (JSONException e) {
                            e.printStackTrace();
                           // progressBar2.setVisibility(View.GONE);
                        }
                      //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(getActivity()).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

    @Override
    public void onResume() {
        super.onResume();
        fetch_matches();
    }
}
