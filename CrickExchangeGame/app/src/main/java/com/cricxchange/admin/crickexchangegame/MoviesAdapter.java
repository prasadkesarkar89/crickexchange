package com.cricxchange.admin.crickexchangegame;

import android.content.Context;
import android.os.CountDownTimer;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import Class_Stucture.Match;
import Utility.URLs;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MyViewHolder> {

    private List<Match> matchList;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
    Context con;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView team_one_logo;
        public TextView team_one_name;
        public TextView match_name;
        public TextView timer;
        public ImageView team_two_logo;
        public TextView team_two_name;

        public MyViewHolder(View view) {
            super(view);
            team_one_logo = (ImageView) view.findViewById(R.id.team_one_logo);
            team_one_name = (TextView) view.findViewById(R.id.team_one_name);
            match_name = (TextView) view.findViewById(R.id.match_name);
            timer = (TextView) view.findViewById(R.id.timer);
            team_two_logo = (ImageView) view.findViewById(R.id.team_two_logo);
            team_two_name = (TextView) view.findViewById(R.id.team_two_name);
        }
    }


    public MoviesAdapter(List<Match> matchList, Context con) {
        this.matchList = matchList;
        this.con = con;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.match_row_temp, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        try {
            Match match = matchList.get(position);
            Date matchDate = sdf.parse(match.getMatch_date().toString());
            Glide.with(con)
                    .load(URLs.IMAGE_LOADER + "" + match.getTeam_one_logo())
                    .into(holder.team_one_logo);
            holder.team_one_name.setText(match.getTeam_one_name());
            holder.match_name.setText(match.getMatch_name());
//        holder.timer.set
            Calendar cal = Calendar.getInstance();
            cal.setTime(matchDate);
            getCountOfDays(match.getMatch_date().toString(),"");
            reverseTimer(cal.getTimeInMillis(),holder.timer);
            Glide.with(con)
                    .load(URLs.IMAGE_LOADER + "" + match.getTeam_one_logo())
                    .into(holder.team_two_logo);
            holder.team_two_name.setText(match.getTeam_two_name());


        }
        catch (Exception e)
        {

        }
    }

    public void reverseTimer(long Seconds, final  TextView tv) {

        new CountDownTimer(Seconds, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);


                int days = (int) (millisUntilFinished / (1000*60*60*24));
                long days1 = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                days1 = days1 /60 * 60;
                days1 = days1 /24;
                tv.setText(days1+"d"+
                        (TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)) +"h"+
                                (TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)) +"m"+
                                        (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))+"s"))));
            }

            public void onFinish() {
                tv.setText("Completed");
            }
        }.start();
    }

    public String getCountOfDays(String createdDateString, String expireDateString) {
        long diff  = 0;
        try {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());

        Calendar cal =  Calendar.getInstance();


        Date today  =  dateFormat.parse(cal.toString());

        Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;

            createdConvertedDate = dateFormat.parse(createdDateString);
            expireCovertedDate = dateFormat.parse(expireDateString);

            Date today1 = new Date();

            todayWithZeroTime = dateFormat.parse(dateFormat.format(today));


        int cYear = 0, cMonth = 0, cDay = 0;

        if (createdConvertedDate.after(todayWithZeroTime)) {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(createdConvertedDate);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);

        } else {
            Calendar cCal = Calendar.getInstance();
            cCal.setTime(todayWithZeroTime);
            cYear = cCal.get(Calendar.YEAR);
            cMonth = cCal.get(Calendar.MONTH);
            cDay = cCal.get(Calendar.DAY_OF_MONTH);
        }


    /*Calendar todayCal = Calendar.getInstance();
    int todayYear = todayCal.get(Calendar.YEAR);
    int today = todayCal.get(Calendar.MONTH);
    int todayDay = todayCal.get(Calendar.DAY_OF_MONTH);
    */

        Calendar eCal = Calendar.getInstance();
        eCal.setTime(expireCovertedDate);

        int eYear = eCal.get(Calendar.YEAR);
        int eMonth = eCal.get(Calendar.MONTH);
        int eDay = eCal.get(Calendar.DAY_OF_MONTH);

        Calendar date1 = Calendar.getInstance();
        Calendar date2 = Calendar.getInstance();

        date1.clear();
        date1.set(cYear, cMonth, cDay);
        date2.clear();
        date2.set(eYear, eMonth, eDay);

            diff = date2.getTimeInMillis() - date1.getTimeInMillis();



        } catch (ParseException e) {
            e.printStackTrace();
        }
        float dayCount = (float) diff / (24 * 60 * 60 * 1000);
        return ("" + (int) dayCount + " Days");
    }

    @Override
    public int getItemCount() {
        return matchList.size();
    }
}
