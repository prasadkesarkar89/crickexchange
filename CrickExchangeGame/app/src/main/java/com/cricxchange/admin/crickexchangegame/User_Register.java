package com.cricxchange.admin.crickexchangegame;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Class_Stucture.User;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;
import butterknife.BindView;
import butterknife.ButterKnife;

public class User_Register extends AppCompatActivity implements View.OnClickListener, GoogleApiClient.OnConnectionFailedListener{

    @BindView(R.id.edt_name)EditText edt_name;
    @BindView(R.id.edt_email)EditText edt_email;
    @BindView(R.id.edt_mobile)EditText edt_mobile;
    @BindView(R.id.edt_password)EditText edt_password;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @BindView(R.id.cordinate_layout) android.support.design.widget.CoordinatorLayout cor;

    @BindView(R.id.btn_signUp)
    Button btn_signUp;

    @BindView(R.id.img_btn_fb)
    ImageView img_btn_fb;


    @BindView(R.id.imgtngplus)
    ImageView imgtngplus;

    private static final int RC_SIGN_IN = 9001;
    private CallbackManager callbackManager;
    private GoogleApiClient mGoogleApiClient;
    private GoogleSignInAccount account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user__register);

        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        btn_signUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Rigister(edt_name.getText().toString(),edt_email.getText().toString(),edt_mobile.getText().toString(),edt_password.getText().toString(),"3");
            }
        });

        img_btn_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppEventsLogger.activateApp(User_Register.this);
                List<String> permissions = new ArrayList<String>();
                permissions.add("public_profile");
                permissions.add("email");
//
                LoginManager.getInstance().logInWithReadPermissions(User_Register.this, permissions);
                LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        GraphRequest request = GraphRequest.newMeRequest(

                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        // Application code
                                        Log.v("LoginActivity", response.toString());

                                        String first_name = "", last_name = "";
                                        String name = "";
                                        try {

                                            //Log.e("GRAPH REQUEST JSON OBJECT-->>", String.valueOf(object));
                                            //Log.e("GRAPH REQUEST RESPONSE-->>", String.valueOf(response));


                                            Log.e("FB id", "FB id");
                                            String facebook_id = object.getString("id");
                                            Log.e("FB id", facebook_id);
                                            //New change:

                                            name = object.getString("name");
                                           /* first_name=object.getString("first_name");
                                            Log.e("FB first_name",first_name);
                                            last_name=object.getString("last_name");
                                            Log.e("FB last_name",last_name);*/
                                            String token = String.valueOf(AccessToken.getCurrentAccessToken().getToken());
                                            Log.e("FB TOken", token);
                                            String email_id = "";

                                            // editor.commit();
                                            if (object.has("email")) {
                                                email_id = object.getString("email");
                                            }
                                            String profileImageUrl = "https://graph.facebook.com/" + facebook_id + "/picture?type=large";
                                            JSONObject login_json = new JSONObject(object.toString());
                                            login_json.accumulate("image_url", profileImageUrl);
                                            login_json.accumulate("user_name", name);
                                            // login_json.accumulate("user_name",first_name+" "+last_name);

                                            // new change: kritika saving useremail id // no email id in case of facebook
                                            String userEmailId = login_json.has("email") ? login_json.getString("email") : null;
                                            //String userMoile   = login_json.has("mobile") ? login_json.getString("mobile") : "";

                                            Rigister(name,userEmailId,"","","1");


                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                            Log.e("eeeee", e.toString());
                                        }
                                        //Toast.makeText(LoginScreenActivity.this, "Congrats " + name + ", you are successfully logged in facebook", Toast.LENGTH_LONG).show();

                                        finish();
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday,link");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        // signedWithFB = false;
                    }

                    @Override
                    public void onError(FacebookException error) {
                        //signedWithFB = false;
                    }
                });
            }
        });

        callbackManager = CallbackManager.Factory.create();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("839393689434-quu7iccemrmkcasmllod917227sfoqv7.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        imgtngplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId())
        {
            case R.id.btn_signUp:
//                Rigister();
                break;
        }

    }

    private void Rigister(final String name,final String email_id,final String mobile_no,final String password,final String user_type) {
        //first getting the values

        progressBar.setVisibility(View.VISIBLE);


         final String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);



        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.URL_REGISTER,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            if (obj.getInt("response_code") == 200) {

//                                User user = new User(
//                                        Integer.parseInt(obj.getString("id")),
//                                        obj.getString("name"),
//                                        obj.getString("email_id"),
//                                        obj.getString("mobile_no")
//                                );
//                                SharedPrefManager.getInstance(getApplicationContext()).userLogin(user);
                                Snackbar snackbar = Snackbar
                                        .make(cor, "Registration Successful!!!!", Snackbar.LENGTH_LONG);

                                snackbar.show();
                                finish();
                                Intent dashBoardForm = new Intent(User_Register.this,DashBoard.class);
                                startActivity(dashBoardForm);
                               // SharedPrefManager.getInstance(getApplicationContext()).SaveAPIKey(String.valueOf(obj.getString("authorization_key")));

                            }
                            else
                            {
                                Snackbar snackbar = Snackbar
                                        .make(cor, "Ooops Getting Error! Try Again"+obj.getString("message"), Snackbar.LENGTH_LONG);

                                snackbar.show();
                            }
//                            if(obj.get)
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressBar.setVisibility(View.GONE);
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("name", name);
                params.put("email_id", email_id);
                params.put("mobile_no", mobile_no);
                params.put("user_type", user_type);
                params.put("device_type", "1");
                params.put("device_id", android_id);
                params.put("password", password);
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(getApplicationContext()).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleResult(result);
        }else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private void handleResult(GoogleSignInResult result) {
        if (result.isSuccess()) {
            account = result.getSignInAccount();
            String name = account.getDisplayName();
            String email = account.getEmail();
            String img_url = account.getPhotoUrl().toString();

            Rigister(name,email,"","","2");
            // textView.setText(" Name /t/n"+ name +" email /t/n"+ email +" img url /t/n"+img_url);
        }
    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }
}
