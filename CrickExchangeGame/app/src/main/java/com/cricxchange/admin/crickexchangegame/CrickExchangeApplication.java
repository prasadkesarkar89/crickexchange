package com.cricxchange.admin.crickexchangegame;

import android.app.Application;
import android.content.Intent;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import com.crashlytics.android.Crashlytics;

import Class_Stucture.Player;
import io.fabric.sdk.android.Fabric;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import Class_Stucture.User;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;

/**
 * Created by Admin on 2/24/2018.
 */

public class CrickExchangeApplication extends Application
{
    static public ArrayList<Player> teamList  = new ArrayList<>();

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        getApiKey();
    }

    private void getApiKey() {
        //first getting the values
        final String username = "api_admin";
        final String password = "aaaaaa";



        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.GETAPIKey,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);

                            if(obj.getInt("response_code") == 200)
                            {
                                SharedPrefManager.getInstance(getApplicationContext()).SaveAPIKey(String.valueOf(obj.getString("authorization_key")));

                            }
//                            if(obj.get)
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("username", username);
                params.put("password", password);
                return params;
            }
        };

        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
