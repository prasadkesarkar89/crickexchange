package com.cricxchange.admin.crickexchangegame;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import Class_Stucture.Match_details;
import Class_Stucture.Player;
import Class_Stucture.PlayerMatch;
import Class_Stucture.Room;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;
import adapter.ListAdapter_match;
import adapter.ListAdapter_rooms;

public class SelectRoom extends AppCompatActivity {

    public GridView recyclerView;
    String match_id, room_id;
    public ListAdapter_rooms listRoom;
    ArrayList<Room> roomList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_room);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent i = getIntent();

        match_id = i.getStringExtra("match_id");
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = (GridView) findViewById(R.id.gridview);
        listRoom = new ListAdapter_rooms(SelectRoom.this, roomList);
        recyclerView.setAdapter(listRoom);

        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Get the GridView selected/clicked item text
                Room rm = roomList.get(position);
                room_id = rm.getId();
                createteam();
                String selectedItem = parent.getItemAtPosition(position).toString();

                // Display the selected/clicked item text and position on TextView
//                tv.setText("GridView item clicked : " +selectedItem
//                        + "\nAt index position : " + position);
            }
        });
        fetch_room();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    private void fetch_room() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);


        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FETCH_ROOM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                Date matchDate, currDate;
                                JSONArray match_array = obj.getJSONArray("rooms_info");
                                for (int i = 0; i < match_array.length(); i++) {

                                    JSONObject matchObj = match_array.getJSONObject(i);
                                    Room rm = gson.fromJson(matchObj.toString(), Room.class);
                                    roomList.add(rm);

                                }


                            } else {

                            }
                            listRoom.notifyDataSetChanged();

//                            if(obj.get)
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressBar2.setVisibility(View.GONE);
                        }
                        //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SelectRoom.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
////                Map<String, String> param = new HashMap<>();
////                param.put("match_id", match_id);
//                return param;
//            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(SelectRoom.this).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(SelectRoom.this).addToRequestQueue(stringRequest);
    }


    private void createteam() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);


        //if everything is fine


        JSONObject sendObject = new JSONObject();
        final JSONArray array = new JSONArray();
        try {

            sendObject.put("user_id", String.valueOf(SharedPrefManager.getInstance(SelectRoom.this).getUser().getId()));
            sendObject.put("match_id", match_id);
            sendObject.put("room_id", room_id);

//            JSONArray array = new JSONArray();
            JSONObject obj = new JSONObject();

            Gson gson = new Gson();
            ArrayList<PlayerMatch> playerList = new ArrayList<>();
            playerList = getPlayerList();
            for (int i = 0; i < playerList.size(); i++) {
                JSONObject obj1 = new JSONObject();

                obj1.put("id", playerList.get(i).getId().toString());
                obj1.put("is_captain", playerList.get(i).getIs_captain().toString());
                obj1.put("is_vc_captain", playerList.get(i).getIs_captain().toString());

                array.put(obj1);

            }

            sendObject.put("players", array);
        } catch (Exception e) {
            e.printStackTrace();
        }

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.CREATE_ROOM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                Date matchDate, currDate;
                                JSONArray match_array = obj.getJSONArray("rooms_info");
                                for (int i = 0; i < match_array.length(); i++) {

                                    JSONObject matchObj = match_array.getJSONObject(i);
                                    Room rm = gson.fromJson(matchObj.toString(), Room.class);
                                    roomList.add(rm);

                                }


                            } else {

                            }
                            listRoom.notifyDataSetChanged();

//                            if(obj.get)
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressBar2.setVisibility(View.GONE);
                        }
                        //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(SelectRoom.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("user_id", String.valueOf(SharedPrefManager.getInstance(SelectRoom.this).getUser().getId()));
                param.put("match_id", match_id);
                param.put("room_id", room_id);
                param.put("players", array.toString());

                ArrayList<PlayerMatch> numbers = getPlayerList();


                Map<String, String> params = new HashMap<String, String>();

                int i = 0;
//                for (String object : numbers) {
//                    params.put("friendnr[" + (i++) + "]", object);
//                    // you first send both data with same param name as friendnr[] ....  now send with params friendnr[0],friendnr[1] ..and so on
//                }

                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(SelectRoom.this).getApiKey());
                return header;
            }
        };





        JsonObjectRequest jsonObjectRequest = new
                JsonObjectRequest(Request.Method.POST,
                        URLs.CREATE_ROOM,
                        sendObject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Log.d("fdsaf", response.toString());
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("safd", error.toString());
                            }
                        }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        HashMap<String, String> headers = new HashMap<>();

                        headers.put("x-api-key", SharedPrefManager.getInstance(SelectRoom.this).getApiKey());
                        return headers;
                    }

                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        Map<String, String> params = new HashMap<>();
                        params.put("user_id", String.valueOf(SharedPrefManager.getInstance(SelectRoom.this).getUser().getId()));
                        params.put("match_id", match_id);
                        params.put("room_id", room_id);
                        params.put("players", array.toString());
                        return params;
                    }
                };



        VolleySingleton.getInstance(SelectRoom.this).addToRequestQueue(jsonObjectRequest);
    }

    ArrayList<PlayerMatch> getPlayerList()
    {
        ArrayList<PlayerMatch> playerList  = new ArrayList<>();
        for (Player p1:CrickExchangeApplication.teamList
             ) {

            PlayerMatch p = new PlayerMatch();
            p.setId(String.valueOf(p1.getPlayer_id()));
            p.setIs_captain("1");
            p.setIs_vc_captain("0");
            playerList.add(p);
        }
        return playerList;
    }
}
