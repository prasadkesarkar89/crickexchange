package com.cricxchange.admin.crickexchangegame;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import Class_Stucture.Match;
import Class_Stucture.Match_details;
import Class_Stucture.Player;
import Class_Stucture.Room;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;
import adapter.ListAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import interfaces.GetPlayerCount;

public class CreateYourTeam extends AppCompatActivity implements GetPlayerCount{

    String match_id,match_name,match_date,match_type;
    public int wkcnt,bcnt,arcnt,bowlcnt;
    ArrayList<Player> players = new ArrayList<Player>();
    ArrayList<Player> playersWK = new ArrayList<Player>();
    ArrayList<Player> playersBat = new ArrayList<Player>();
    ArrayList<Player> playersAR = new ArrayList<Player>();
    ArrayList<Player> playersBowl = new ArrayList<Player>();
    ArrayList<Player> teamList  = new ArrayList<Player>();

    ArrayList<Room> roomList = new ArrayList<Room>();
    ListAdapter boxAdapter;

    @BindView(R.id.txt_match_name)
    TextView txt_match_name;

    @BindView(R.id.btn_bowl)
    Button btn_bowl;

    @BindView(R.id.btn_wk)
    Button btn_wk;
    @BindView(R.id.btn_bat)
    Button btn_bat;
    @BindView(R.id.btn_ar)
    Button btn_ar;

    @BindView(R.id.txt_type)
            TextView txt_type;


    @BindView(R.id.timer)
            TextView timer;


    @BindView(R.id.txtwk)
            TextView txtwk;
    @BindView(R.id.txtbat)
    TextView txtbat;
    @BindView(R.id.txtar)
    TextView txtar;
    @BindView(R.id.txtbowl)
    TextView txtbowl;


    @BindView(R.id.fetchrrom)
    Button fetchrrom;

    ListView lvMain ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_your_team);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        boxAdapter = new ListAdapter(this, players);
        lvMain = (ListView) findViewById(R.id.player_list);
        lvMain.setAdapter(boxAdapter);
        lvMain.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
         Intent data = getIntent();
         match_id = data.getStringExtra("match_id");
        match_name = data.getStringExtra("match_name");
        match_date  = data.getStringExtra("match_date");
        match_type  = data.getStringExtra("match_type");
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }


        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Player p  = players.get(i);

                teamList.add(p);


            }
        });
        txt_match_name.setText(match_name.toString());

        btn_wk.setBackgroundResource(R.drawable.oval_select);
        txtwk.setBackgroundResource(R.drawable.oval_shape_textselect);
        txt_type.setText("PICK 1 WEECKET-KEEPER");
        txtwk.setText(String.valueOf(wkcnt));
        txtbowl.setText(String.valueOf(bowlcnt));
        txtar.setText(String.valueOf(arcnt));
        txtbat.setText(String.valueOf(bcnt));

        btn_bowl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_bowl.setBackgroundResource(R.drawable.oval_select);
                txtbowl.setBackgroundResource(R.drawable.oval_shape_textselect);
                btn_wk.setBackgroundResource(R.drawable.oval_shape);
                btn_bat.setBackgroundResource(R.drawable.oval_shape);
                btn_ar.setBackgroundResource(R.drawable.oval_shape);
                txtbat.setBackgroundResource(R.drawable.oval_shape_text);
                txtwk.setBackgroundResource(R.drawable.oval_shape_text);
                txtar.setBackgroundResource(R.drawable.oval_shape_text);
                txt_type.setText("PICK BOWLER");
                players.clear();
                players.addAll(playersBowl);
                boxAdapter.notifyDataSetChanged();
            }
        });
        btn_wk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_bowl.setBackgroundResource(R.drawable.oval_shape);
                btn_wk.setBackgroundResource(R.drawable.oval_select);
                txtwk.setBackgroundResource(R.drawable.oval_shape_textselect);
                btn_bat.setBackgroundResource(R.drawable.oval_shape);
                btn_ar.setBackgroundResource(R.drawable.oval_shape);
                txtbat.setBackgroundResource(R.drawable.oval_shape_text);
                txtbowl.setBackgroundResource(R.drawable.oval_shape_text);
                txtar.setBackgroundResource(R.drawable.oval_shape_text);

                txt_type.setText("PICK 1 WEECKET-KEEPER");
                players.clear();
                players.addAll(playersWK);
                boxAdapter.notifyDataSetChanged();
            }
        });
        btn_bat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_bowl.setBackgroundResource(R.drawable.oval_shape);
                btn_wk.setBackgroundResource(R.drawable.oval_shape);
                btn_bat.setBackgroundResource(R.drawable.oval_select);
                txtbat.setBackgroundResource(R.drawable.oval_shape_textselect);
                btn_ar.setBackgroundResource(R.drawable.oval_shape);

                txtbowl.setBackgroundResource(R.drawable.oval_shape_text);
                txtwk.setBackgroundResource(R.drawable.oval_shape_text);
                txtar.setBackgroundResource(R.drawable.oval_shape_text);
                txt_type.setText("PICK BATSMEN");
                players.clear();
                players.addAll(playersBat);
                boxAdapter.notifyDataSetChanged();
            }
        });
        btn_ar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btn_bowl.setBackgroundResource(R.drawable.oval_shape);
                btn_wk.setBackgroundResource(R.drawable.oval_shape);
                btn_bat.setBackgroundResource(R.drawable.oval_shape);
                btn_ar.setBackgroundResource(R.drawable.oval_select);
                txtar.setBackgroundResource(R.drawable.oval_shape_textselect);
                txtbat.setBackgroundResource(R.drawable.oval_shape_text);
                txtwk.setBackgroundResource(R.drawable.oval_shape_text);
                txtbowl.setBackgroundResource(R.drawable.oval_shape_text);
                txt_type.setText("PICK ALL-ROUNDER");
                players.clear();
                players.addAll(playersAR);
                boxAdapter.notifyDataSetChanged();
            }
        });


        if(match_type.toString().equals("3")) {
            try {
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.getDefault());
                SimpleDateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

                Calendar cal = Calendar.getInstance();


//            Date today  =  dateFormat.parse(cal.toString());

                Date createdConvertedDate = null, expireCovertedDate = null, todayWithZeroTime = null;

                createdConvertedDate = dateFormat.parse(match_date);
//        holder.timer.set
                int days = (int) (createdConvertedDate.getTime() / (1000 * 60 * 60 * 24));
                int currentdays = (int) (cal.getTimeInMillis() / (1000 * 60 * 60 * 24));
                long daytoshow = days - currentdays;

                if (match_type.equals("3")) {
                    if (daytoshow < 2) {
                        reverseTimer(createdConvertedDate.getTime(), timer, cal.getTimeInMillis());
                    } else {
                        DateFormat originalFormat = new SimpleDateFormat("MMMM dd, yyyy", Locale.ENGLISH);
                        timer.setText(originalFormat.format(createdConvertedDate));
                    }
                    timer.setVisibility(View.VISIBLE);
                }
                else {
                    timer.setVisibility(View.INVISIBLE);
                }
            }catch (Exception e)
            {

            }
        }

        fetch_match_details();


        fetchrrom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent room = new Intent(CreateYourTeam.this,SelectRoom.class);
                room.putExtra("match_id",match_id);
                startActivity(room);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
//                Toast.makeText(getApplicationContext(),"Back button clicked", Toast.LENGTH_SHORT).show();
                break;
        }
        return true;
    }

    private void fetch_match_details() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);




        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FETCH_MATCH_details,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                Date matchDate , currDate;
                                JSONObject match_array = obj.getJSONObject("match_details");
//                                for (int i = 0 ; i< match_array.length(); i++)
//                                {
//
////                                    JSONObject matchObj = match_array.getJSONObject(i);
//
////                                    matchDate = sdf.parse(matchObj.getString("match_date").toString());
////                                    currDate = new Date();
////                                    String cur = sdf.format(new Date());
////                                    Calendar cal = Calendar.getInstance();
////                                    currDate.setTime(cal.getTimeInMillis());
////
////                                    if(matchDate.compareTo(currDate) == 0)
////                                    {
////                                        Match m_obje = gson.fromJson(matchObj.toString(), Match.class);
////                                        matchListLive.add(m_obje);
////                                    }
////                                    else if(matchDate.compareTo(currDate) < 0)
////                                    {
////                                        Match m_obje = gson.fromJson(matchObj.toString(), Match.class);
////                                        matchResult.add(m_obje);
////                                    }
////                                    else if(matchDate.compareTo(currDate) > 0)
////                                    {
////                                        Match m_obje = gson.fromJson(matchObj.toString(), Match.class);
////                                        matchListFixture.add(m_obje);
////                                    }
//
//
//                                }

                                Match_details m_obje = gson.fromJson(match_array.toString(), Match_details.class);
                                System.out.println("sdgf");

                                players.addAll(m_obje.getTeam_one_players());
                                players.addAll(m_obje.getTeam_two_players());

//                                matchList.clear();
//                                matchList = matchListFixture;
//                                mAdapter.notifyDataSetChanged();

                            }
                            else
                            {

                            }

                            for (Player p:players
                                 ) {
                                  if(p.getPlayer_role().equalsIgnoreCase("Wicket Keeper"))
                                  {
                                      playersWK.add(p);
                                  }
                                  else if(p.getPlayer_role().equalsIgnoreCase("Batsman"))
                                  {
                                      playersBat.add(p);
                                  }
                                  else if(p.getPlayer_role().equalsIgnoreCase("All Rounder"))
                                  {
                                      playersAR.add(p);
                                  }
                                  else if(p.getPlayer_role().equalsIgnoreCase("Bowler"))
                                  {
                                      playersBowl.add(p);
                                  }

                            }
                            players.clear();
                            players.addAll(playersWK);
                            boxAdapter.notifyDataSetChanged();

//                            if(obj.get)
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressBar2.setVisibility(View.GONE);
                        }
                        //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CreateYourTeam.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> param = new HashMap<>();
                param.put("match_id", match_id);
                return param;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(CreateYourTeam.this).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(CreateYourTeam.this).addToRequestQueue(stringRequest);
    }

    private void fetch_room() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);




        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FETCH_ROOM,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                Date matchDate , currDate;
                                JSONArray match_array = obj.getJSONArray("rooms_info");
                                for (int i = 0 ; i< match_array.length(); i++)
                                {

                                    JSONObject matchObj = match_array.getJSONObject(i);
                                    Room rm = gson.fromJson(matchObj.toString(), Room.class);
                                    roomList.add(rm);

                                }

                                Match_details m_obje = gson.fromJson(match_array.toString(), Match_details.class);
                                System.out.println("sdgf");

                                players.addAll(m_obje.getTeam_one_players());
                                players.addAll(m_obje.getTeam_two_players());

//                                matchList.clear();
//                                matchList = matchListFixture;
//                                mAdapter.notifyDataSetChanged();

                            }
                            else
                            {

                            }

                            for (Player p:players
                                    ) {
                                if(p.getPlayer_role().equalsIgnoreCase("Wicket Keeper"))
                                {
                                    playersWK.add(p);
                                }
                                else if(p.getPlayer_role().equalsIgnoreCase("Batsman"))
                                {
                                    playersBat.add(p);
                                }
                                else if(p.getPlayer_role().equalsIgnoreCase("All Rounder"))
                                {
                                    playersAR.add(p);
                                }
                                else if(p.getPlayer_role().equalsIgnoreCase("Bowler"))
                                {
                                    playersBowl.add(p);
                                }

                            }
                            players.clear();
                            players.addAll(playersWK);
                            boxAdapter.notifyDataSetChanged();

//                            if(obj.get)
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressBar2.setVisibility(View.GONE);
                        }
                        //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(CreateYourTeam.this, error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
////                Map<String, String> param = new HashMap<>();
////                param.put("match_id", match_id);
//                return param;
//            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(CreateYourTeam.this).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(CreateYourTeam.this).addToRequestQueue(stringRequest);
    }

    public void reverseTimer(long Seconds, final  TextView tv,final long todayTime) {

        new CountDownTimer(Seconds, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000);


                int days = (int) (millisUntilFinished / (1000*60*60*24));
                int currentdays = (int) (todayTime / (1000*60*60*24));

                long days1 = TimeUnit.MILLISECONDS.toDays(millisUntilFinished);
                long daytoshow = days - currentdays;
                days1 = days1 /60 * 60;
                days1 = days1 /24;
                tv.setText(daytoshow+"d"+
                        ((TimeUnit.MILLISECONDS.toHours(millisUntilFinished) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toHours(todayTime) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(todayTime)))) +"h"+
                                ((TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millisUntilFinished)))- ((TimeUnit.MILLISECONDS.toMinutes(todayTime) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(todayTime))))+"m"+
                                        (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))+"s"))));
            }

            public void onFinish() {
                tv.setText("Completed");
            }
        }.start();
    }

    @Override
    public void playerCount(int i,String type) {

            switch (type)
            {
                case  "Wicket Keeper":
                     wkcnt = wkcnt  + i;
                     txtwk.setText(String.valueOf(wkcnt));
                    break;
                case  "Batsman":
                    bcnt = bcnt  + i;
                    txtbat.setText(String.valueOf(bcnt));
                    break;
                case  "All Rounder":
                    arcnt = arcnt  + i;
                    txtar.setText(String.valueOf(bcnt));
                    break;
                case  "Bowler":
                    bowlcnt = bowlcnt  + i;
                    txtbowl.setText(String.valueOf(bowlcnt));
                    break;

            }

    }
}
