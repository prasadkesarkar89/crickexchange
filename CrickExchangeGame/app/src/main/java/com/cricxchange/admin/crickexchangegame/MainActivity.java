package com.cricxchange.admin.crickexchangegame;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Class_Stucture.Match;
import Storage_Structure.SharedPrefManager;
import Utility.URLs;
import Utility.VolleySingleton;
import adapter.ListAdapter_match;
import adapter.MatchAdapter;

public class MainActivity extends Fragment {
    public ArrayList<Match> matchListFixture = new ArrayList<>();
    public ArrayList<Match> matchListLive = new ArrayList<>();
    public ArrayList<Match> matchResult = new ArrayList<>();

    public ArrayList<Match> matchList = new ArrayList<>();
    public ArrayList<Match> allmatchList = new ArrayList<>();
    public ListView recyclerView;
    public MoviesAdapter mAdapter;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TabLayout tab_layout;
    public ListAdapter_match listMact;

    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
//    Date matchDate , currDate;

    public static MatchList newInstance(String param1, String param2) {
        MatchList fragment = new MatchList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public MainActivity() {
        // Required empty public constructor
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.activity_main, container, false);
        // progressBar2 = (ProgressBar) v.findViewById(R.id.progressBar2);
        recyclerView = (ListView) v.findViewById(R.id.recycler_view);
        tab_layout = (TabLayout) v.findViewById(R.id.tab_layout);

//        mAdapter = new MoviesAdapter(matchList,getActivity());
        listMact = new ListAdapter_match(getActivity(),matchList);


        recyclerView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Match match  = null;

                if(tab_layout.getSelectedTabPosition() == 0)
                {
                    match = matchListFixture.get(i);
                }
                else if(tab_layout.getSelectedTabPosition() == 1)
                {
                    match = matchListLive.get(i);
                }
                else if(tab_layout.getSelectedTabPosition() == 2)
                {
                    match = matchResult.get(i);
                }
                Intent createTeam = new Intent(getActivity(),CreateYourTeam.class);
                createTeam.putExtra("match_id",match.getMatch_id());
                createTeam.putExtra("match_name",match.getMatch_name());
                createTeam.putExtra("match_date",match.getMatch_date());
                createTeam.putExtra("match_type",String.valueOf(match.getTypematch()));

                startActivity(createTeam);
            }
        });
//        recyclerView.setHasFixedSize(true);

        // vertical RecyclerView
        // keep movie_list_row.xml width to `match_parent`
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());

        // horizontal RecyclerView
        // keep movie_list_row.xml width to `wrap_content`
        // RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);

//        recyclerView.setLayoutManager(mLayoutManager);

        // adding inbuilt divider line
//        recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL,40,true));

        // adding custom divider line with padding 16dp
        // recyclerView.addItemDecoration(new MyDividerItemDecoration(this, LinearLayoutManager.HORIZONTAL, 16));
//        recyclerView.setItemAnimator(new DefaultItemAnimator());

        recyclerView.setAdapter(listMact);

        // row click listener

        tab_layout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener(){
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                int position = tab.getPosition();
                if(position == 0)
                {
//                    matchList.clear();
//                    matchList = matchListFixture;
                    listMact = new ListAdapter_match(getActivity(),matchListFixture);
                    recyclerView.setAdapter(listMact);

                }
                else  if(position == 1)
                {
                    listMact = new ListAdapter_match(getActivity(),matchListLive);
                    recyclerView.setAdapter(listMact);
                }
                else if(position == 2)
                {
                    listMact = new ListAdapter_match(getActivity(),matchResult);
                    recyclerView.setAdapter(listMact);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), recyclerView, new RecyclerTouchListener.ClickListener() {
//            @Override
//            public void onClick(View view, int position) {
//                int tab_position=tab_layout.getSelectedTabPosition();
//                Match match = matchList.get(position);
////                if(tab_position == 0)
////                {
////                    match = matchListFixture.get(position);
////                }
////                else  if(tab_position == 1)
////                {
////                    match = matchListLive.get(position);
////                }
////                else if(tab_position == 2)
////                {
////                    match = matchResult.get(position);
////                }
//
//                Intent createTeam = new Intent(getActivity(),CreateYourTeam.class);
//                createTeam.putExtra("match_id",match.getMatch_id());
//                startActivity(createTeam);
//
////                Toast.makeText(getActivity(), match.getTitle() + " is selected!", Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onLongClick(View view, int position) {
//
//            }
//        }));

        fetch_matches();

//        divideList(matchList);
        return v;
    }

    public void divideList(ArrayList<Match> matchList1)
    {
        for (Match m:matchList1
             ) {
            if (m.getTypematch() == 1)
            {
                matchResult.add(m);
            }
            else  if(m.getTypematch() ==2)
            {
                matchListLive.add(m);
            }
            else if(m.getTypematch() == 3)
            {
                matchListFixture.add(m);
            }

        }

    }


    /**
     * Prepares sample data to provide data set to adapter
     */

    public void fetch_matches() {
        //first getting the values

        //progressBar2.setVisibility(View.VISIBLE);




        //if everything is fine
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URLs.FETCH_MATCH,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // progressBar.setVisibility(View.GONE);
                        try {
                            //converting response to json object
                            JSONObject obj = new JSONObject(response);
                            matchListFixture.clear();
                            matchResult.clear();;
                            matchList.clear();
                            matchListLive.clear();
                            if (obj.getInt("response_code") == 200) {

                                Gson gson = new Gson();
                                Date matchDate , currDate;
                                JSONArray match_array = obj.getJSONArray("matches_info");
                                for (int i = 0 ; i< match_array.length(); i++)
                                {

                                    JSONObject matchObj = match_array.getJSONObject(i);

                                    matchDate = sdf.parse(matchObj.getString("match_date").toString());
                                    currDate = new Date();
                                    String cur = sdf.format(new Date());
                                    Calendar cal = Calendar.getInstance();
                                    currDate.setTime(cal.getTimeInMillis());

                                    Match m_obje = gson.fromJson(matchObj.toString(), Match.class);
                                     if(matchDate.compareTo(currDate) == 0)
                                     {
                                         m_obje.setTypematch(1);
                                         matchResult.add(m_obje);
                                     }
                                     else if(matchDate.compareTo(currDate) < 0)
                                     {
                                         m_obje.setTypematch(2);
                                         matchListLive.add(m_obje);
                                     }
                                     else if(matchDate.compareTo(currDate) > 0)
                                     {
                                         m_obje.setTypematch(3);
                                         matchListFixture.add(m_obje);
                                         matchList.add(m_obje);
                                     }


                                }

//                                matchList.clear();
//                                matchList = matchListFixture;
                                listMact.notifyDataSetChanged();
//                                divideList(matchList);

                            }
                            else
                            {

                            }
//                            if(obj.get)
                        } catch (Exception e) {
                            e.printStackTrace();
                            // progressBar2.setVisibility(View.GONE);
                        }
                        //  progressBar2.setVisibility(View.GONE);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }) {


            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> header = new HashMap<>();
                header.put("x-api-key", SharedPrefManager.getInstance(getActivity()).getApiKey());
                return header;
            }
        };

        VolleySingleton.getInstance(getActivity()).addToRequestQueue(stringRequest);
    }

//    private void prepareMovieData() {
//        Movie movie = new Movie("Mad Max: Fury Road", "Action & Adventure", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("Inside Out", "Animation, Kids & Family", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("Star Wars: Episode VII - The Force Awakens", "Action", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("Shaun the Sheep", "Animation", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("The Martian", "Science Fiction & Fantasy", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("Mission: Impossible Rogue Nation", "Action", "2015");
//        movieList.add(movie);
//
//        movie = new Movie("Up", "Animation", "2009");
//        movieList.add(movie);
//
//        movie = new Movie("Star Trek", "Science Fiction", "2009");
//        movieList.add(movie);
//
//        movie = new Movie("The LEGO Movie", "Animation", "2014");
//        movieList.add(movie);
//
//        movie = new Movie("Iron Man", "Action & Adventure", "2008");
//        movieList.add(movie);
//
//        movie = new Movie("Aliens", "Science Fiction", "1986");
//        movieList.add(movie);
//
//        movie = new Movie("Chicken Run", "Animation", "2000");
//        movieList.add(movie);
//
//        movie = new Movie("Back to the Future", "Science Fiction", "1985");
//        movieList.add(movie);
//
//        movie = new Movie("Raiders of the Lost Ark", "Action & Adventure", "1981");
//        movieList.add(movie);
//
//        movie = new Movie("Goldfinger", "Action & Adventure", "1965");
//        movieList.add(movie);
//
//        movie = new Movie("Guardians of the Galaxy", "Science Fiction & Fantasy", "2014");
//        movieList.add(movie);
//
//        // notify adapter about data set changes
//        // so that it will render the list with new data
//        mAdapter.notifyDataSetChanged();
//    }

}
