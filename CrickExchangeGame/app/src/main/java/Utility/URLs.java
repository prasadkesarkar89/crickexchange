package Utility;

/**
 * Created by Admin on 2/24/2018.
 */

public class URLs {

    private static final String ROOT_URL = "http://www.technovador.com/cricket";

    public static final String GETAPIKey = ROOT_URL + "/api/users/get_api_key";
    public static final String URL_REGISTER = ROOT_URL + "/api/users/register";
    public static final String URL_LOGIN= ROOT_URL + "/api/users/login";

    public static final String IMAGE_LOADER = ROOT_URL + "/resources/admin/uploads/teams_logos/";
    public  static final String FETCH_MATCH = ROOT_URL+"/api/matches/get_all_matches";
    public  static final String FETCH_MATCH_details = ROOT_URL+"/api/matches/get_match_details";
    public  static final String FETCH_ROOM = ROOT_URL+"/api/rooms/get_all_rooms";
    public  static final String CREATE_ROOM = ROOT_URL+"/api/users/create_user_team";

}
